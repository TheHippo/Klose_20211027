//go:build !postgres
// +build !postgres

package database

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func openDB() (*gorm.DB, error) {
	filename := getEnv("SQLITE_FILE", "test.db")
	return gorm.Open(sqlite.Open(filename), &gorm.Config{})

}
