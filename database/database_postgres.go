//go:build postgres
// +build postgres

package database

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func openDB() (*gorm.DB, error) {
	dsn := getEnv("POSTGRESQL_DSN", "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Europe/Berlin")
	return gorm.Open(postgres.Open(dsn), &gorm.Config{})

}
