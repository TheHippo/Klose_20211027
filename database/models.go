package database

import "gorm.io/gorm"

var baseCategories = [3]string{"Exercise", "Education", "Recipe"}

// we are not using gorm.Model because of all the auto-update
// soft-delete features we don't need

type Category struct {
	ID   uint64 `gorm:"primaryKey;autoIncrement"`
	Name string `gorm:"size:200"`
}

type Video struct {
	ID               uint64 `gorm:"primaryKey;autoIncrement"`
	Title            string `gorm:"size:200"`
	OriginalFileName string `gorm:"size:200"`
	FileName         string `gorm:"size:200"`
	CategoryID       uint64
	Category         Category
	Thumbnail256     []byte
	Thumbnail128     []byte
	Thumbnail64      []byte
	// this is personal thing, but storing files in a database is a
	// stupid thing to do. It should on a filesystem so the web server
	// could deliver them. Instead the webserver, the application
	// and the database are involved
}

// ensureBaseCategories inserts the default categories into the
// database if they don't exist already
func ensureBaseCategories(db *gorm.DB) {
	for _, base := range baseCategories {
		var category Category
		db.FirstOrCreate(&category, Category{
			Name: base,
		})
	}
}
