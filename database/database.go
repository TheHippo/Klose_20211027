package database

import (
	"log"
	"os"

	"gorm.io/gorm"
)

// OpenAndPrepareDatabase opens the database and
// makes sure all tables are present and the default
// categories are available
func OpenAndPrepareDatabase() *gorm.DB {
	db, err := openDB()
	if err != nil {
		log.Fatalf("Failed to connect database: %s\n", err)
	}
	db.AutoMigrate(&Category{})
	db.AutoMigrate(&Video{})
	ensureBaseCategories(db)
	return db
}

// getEnv get key environment variable if exist otherwise return defalutValue
func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}
