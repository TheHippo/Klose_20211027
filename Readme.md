# Video upload server

## Backend

The backend is written in Go. It uses the following libraries:

* Database access / ORM: [gorm](https://gorm.io/)
* Routing: [chi](https://github.com/go-chi/chi)

**Platform support:** It's only tested on Linux, but I'm quite confident that it will work fine on most
Posix systems. There is a good chance it works on Windows as well.

If you run this locally make sure you have `ffmpeg` installed otherwise the thumbnails cannot be generated. (See [thumbnail generation](#thumbnail-generation))

### Building the backend

Make sure you have all the dependencies installed:

    go mod download

Build the server:

    go build -o server gitlab.com/TheHippo/Klose_20211027

After starting the server it should be available at port [localhost:3000](http://localhost:3000)

### Database support

The backend supports [SQLite](https://www.sqlite.org/index.html) and [PostgreSQL](https://www.postgresql.org/). This is a compile-time
decision, so changing databases requires a re-compilation.

The default database is SQLite, the database is stored next to the binary as `test.db`. To change the filename and path set the 
environment variable `SQLITE_FILE` to the desired location.

To switch to PostgreSQL the backend need to be compiled with a `postgres` tag:

    go build -tags=postgres -o server gitlab.com/TheHippo/Klose_20211027

The database source name can be set through the `POSTGRESQL_DSN` environment variable.

**Note:** The SQLite version requires `cgo`, whereas the PostgreSQL implementation is a pure Go and therefore a lot easier to cross-compile or to
build completely static.

### Production mode

When compiling the backend with the `production` tag enabled all the HTML templates will be parsed and checked during start up of the backend.
Changing anything in the template requires a restart of the server. In the default mode templates will be read and parsed on each request.

	go build -tags=production -o server gitlab.com/TheHippo/Klose_20211027

### Upload protection

To ensure that videos can only be uploaded through the upload formular a simple check is implemented:

* On server start-up a random salt value is generated.
* When showing the upload formular a random token is generated and SHA256 hashed with the salt value. Both the token and the hash are included into the formular.
* When accepting the upload request it is checked whether the hash can be regenerated from the token and the salt value, that is unknown to the user.
* When the upload is completed the token is saved in memory and cannot be used again.

This should be fine until a user finds the salt value or SHA256 gets cracked. Due to the salt value generation on start up an upload formular becomes invalid between server
restarts. This can still be misused, but at least requires the attacker to read the actual HTML formular for every upload.

This implementation obviously works only when there is a single instance of the backend running or it is made sure a user always connects to the same instance.

## Frontend

There is no huge Javascript framework involved. Styling is built with [Tailwind CSS](https://tailwindcss.com/). The video player is implemented in plain
JavaScript / ECMAscript and minified with [esbuild](https://esbuild.github.io/).

Installing and building should be doable with `npm` or `yarn`, although just `yarn` was tested.

The code can be found in the `frontend` folder and all generated files will be in `static`.

### Building the frontend

For production

	yarn
	yarn css:build:production
	yarn js:build:production

Development CSS build and watching the files:

	yarn build:dev

Development JavaScript build:

	yarn css:build:dev


## Thumbnail generation

The backend uses `ffmpeg` for generating the thumbnail with a command similar to this one:

	ffmpeg -ss 00:00:01.00 -i input.mp4 -vf 'scale=256:256:force_original_aspect_ratio=increase' -vframes 1 output.jpg

## Docker

* The `Dockerfiles` uses multi stage build to keep the Docker images as small as possible. There is a stage for the Go build, for the frontend build and the
final image.
* `ffmpeg` gets installed as a static build from a third party, this keeps the image size down by a half but is somewhat a security issue.

The tags for the Go compiler can be passed via build args name `GOTAGS` (default is: `production` and `postgres`):

	docker build --build-arg GOTAGS=production,sqlite --pull --tag videouploader:latest .


## Docker compose

There are three `docker-compose` files. One for the basic settings, one for running the PostgreSQL version and one for running the SQLite version.

To build and start running the SQLite version:

	docker-compose -f docker-compose.yaml -f docker-compose-sqlite.yaml up

To build and start the PostgreSQL version:

	docker-compose -f docker-compose.yaml -f docker-compose-postgres.yaml up

**Note:** Depending on the speed of your computer there might be an issue, when PostgreSQL isn't actually ready when the container signals it is, which makes the server
fail, because it cannot reach the database. In this case start the database before starting the backend

	docker-compose -f docker-compose.yaml -f docker-compose-postgres.yaml up -d database
	docker-compose -f docker-compose.yaml -f docker-compose-postgres.yaml log -f
	# wait for the database to be ready --> CTRL + C
	docker-compose -f docker-compose.yaml -f docker-compose-postgres.yaml up server
