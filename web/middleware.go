package web

import (
	"context"
	"net/http"

	"gorm.io/gorm"
)

type contextKey uint

const (
	_ contextKey = iota //prevents accidential using 0
	dbKey
)

// databaseMiddleware injects the database reference into the request
func databaseMiddleware(db *gorm.DB) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), dbKey, db)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

// getDatabase extracts the database reference from the request
// only use this if you are sure the database reference is in there
func getDatabase(req *http.Request) *gorm.DB {
	return req.Context().Value(dbKey).(*gorm.DB)
}
