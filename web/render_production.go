//go:build production
// +build production

package web

import (
	"fmt"
	"log"
	"text/template"
)

var templates = map[string]*template.Template{}

// init parses all templates during startup
func init() {
	pages, layouts, err := listFiles()
	if err != nil {
		log.Fatalf("Could not read templates: %s\n", err)
		return
	}
	for name, fileName := range pages {
		files := append([]string{fileName}, layouts...)
		tmpl := template.Must(template.ParseFiles(files...))
		templates[name] = tmpl
	}
	log.Println("Compiled all templates")
}

// getTemplate return the template by its name or an error if it
// does not exit
func getTemplate(name string) (*template.Template, error) {
	tmpl, exists := templates[name]
	if !exists {
		return nil, fmt.Errorf("could not find template %s", name)
	}
	return tmpl, nil
}
