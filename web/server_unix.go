//go:build aix || darwin || dragonfly || freebsd || linux || netbsd || openbsd || solaris
// +build aix darwin dragonfly freebsd linux netbsd openbsd solaris

package web

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// StartServer starts a http.Server and shuts it down gracefully, allowing to serve
// running requests for the timeout period
// the cleanup function will be called after the server stopped
func StartServer(server http.Server, timeout time.Duration, cleanup func()) {
	// fixed size, so its blocks
	done := make(chan os.Signal, 1)
	// the reasons we want the server to stop
	// do not notify on SIGKILL, because a blocking cleanup would prevent
	// the process from ever going down
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		// starting the server in a separate go routine, this blocks
		// as long as the server is running
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Could not bind server: %s\n", err)
		}
	}()
	log.Println("Started server")

	// waiting for the signal to stop the server
	<-done

	log.Println("Stopped server")

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer func() {
		log.Println("Cleaning up")
		cancel()
		cleanup()
	}()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Could not shut down the server: %v\n", err)
	}
	log.Println("Server shutdown successful")
}
