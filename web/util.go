package web

import (
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"sync"
)

var csrfSalt = randomString(20)

var retiredTokens = map[string]struct{}{}
var retiredLock = sync.RWMutex{}

// validSize checks if a requested thumbnail size in within the supported sizes
func validSize(size int64, sizes []int64) bool {
	for _, s := range sizes {
		if size == s {
			return true
		}
	}
	return false
}

// createThumbnail creates thumbnail from a video file, each side of the thumbnail is at least `size` large
func createThumbnail(input string, size int64) ([]byte, error) {
	// generate a file name for the temporary file
	tempOut := os.TempDir() + "/" + randomString(20) + ".jpg"
	// make sure it gets deleted when we're done
	defer os.Remove(tempOut)

	// build ffmpeg command
	commandString := fmt.Sprintf("ffmpeg -ss 00:00:01.00 -i %s -vf scale=%d:%d:force_original_aspect_ratio=increase -vframes 1 %s", input, size, size, tempOut)
	splits := strings.Split(commandString, " ")
	cmd := exec.Command(splits[0], splits[1:]...)
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	// open the temp file and read it to memory
	f, err := os.Open(tempOut)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return io.ReadAll(f)
}

// randomString return a random string to use as a file name
// or crsf token
// this isn't 100% guaranteed to be unique but should
// be good enough for now
func randomString(length uint) string {
	b := make([]byte, length)
	if _, err := rand.Read(b); err != nil {
		// hell froze over, or this codes runs super early in the boot process
		log.Fatalf("Could read rand.Read: %s\n", err)
		return ""
	}
	return fmt.Sprintf("%X", b)
}

// returnError renders an error page and also logs the error
func returnError(w http.ResponseWriter, code int, message string, params ...interface{}) {
	log.Printf(message+"\n", params...)
	http.Error(w, fmt.Sprintf(message, params...), code)
}

// generateSecurityToken generates a token and salted hash value of that token
func generateSecurityToken() (token string, hash string) {
	token = randomString(20)
	hash = fmt.Sprintf("%x", sha256.Sum256([]byte(csrfSalt+token)))
	return
}

// checkSecurity checks if a hash was from the token was generated with the right salt
func checkSecurity(hash, token string) bool {
	retiredLock.RLock()
	defer retiredLock.RUnlock()
	if _, exists := retiredTokens[token]; exists {
		return false
	}
	calcHash := fmt.Sprintf("%x", sha256.Sum256([]byte(csrfSalt+token)))
	return calcHash == hash
}

// retireToken make sure a token can't be reused
func retireToken(token string) {
	retiredLock.Lock()
	defer retiredLock.Unlock()
	retiredTokens[token] = struct{}{}
}
