package web

import (
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	"github.com/go-chi/chi/v5"
	"gitlab.com/TheHippo/Klose_20211027/database"
	"gorm.io/gorm"
)

// max size for a form, 200 MB
const maxSize = 200 << 20

var thumbSizes = [3]int64{64, 126, 256}

// BuildRoutes setup all the routes of the application
func BuildRoutes(r *chi.Mux, db *gorm.DB) *chi.Mux {
	r.Use(databaseMiddleware(db))
	r.Use()
	r.Get("/", index)
	r.Route("/upload", func(r chi.Router) {
		r.Get("/", uploadPage)
		r.Post("/", uploadHandler)
	})
	r.Get("/thumbnail/{videoID:[0-9]+}/{size:[0-9]+}.jpg", thumbnail)

	staticFs := http.FileServer(http.Dir("./static/"))
	r.Handle("/static/*", http.StripPrefix("/static/", staticFs))

	mediaFs := http.FileServer(http.Dir("./media/"))
	r.Handle("/media/*", http.StripPrefix("/media/", mediaFs))

	return r
}

// thumbnail returns the thumbnail file for a video
func thumbnail(w http.ResponseWriter, r *http.Request) {
	size, err := strconv.ParseInt(chi.URLParam(r, "size"), 10, 64)
	if err != nil {
		returnError(w, http.StatusBadRequest, "Invalid size: %s", err)
		return
	}

	if !validSize(size, thumbSizes[:]) {
		returnError(w, http.StatusBadRequest, "Invalid size")
		return
	}
	videoID, err := strconv.ParseInt(chi.URLParam(r, "videoID"), 10, 64)
	if err != nil {
		returnError(w, http.StatusBadRequest, err.Error())
		return
	}
	var video database.Video
	result := getDatabase(r).First(&video, videoID)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		returnError(w, http.StatusBadRequest, result.Error.Error())
		return
	}
	w.Header().Set("Content-Type", "image/jpeg")
	switch size {
	case thumbSizes[0]:
		w.Write(video.Thumbnail64)
	case thumbSizes[1]:
		w.Write(video.Thumbnail128)
	case thumbSizes[2]:
		w.Write(video.Thumbnail256)
	}

}

// uploadPage show the upload form
func uploadPage(w http.ResponseWriter, r *http.Request) {
	var categories []database.Category
	getDatabase(r).Find(&categories)
	token, hash := generateSecurityToken()
	renderHTML(w, "upload", map[string]interface{}{
		"Categories": categories,
		"Hash":       hash,
		"Token":      token,
	})
}

// uploadHandler recieves the uploaded video files
func uploadHandler(w http.ResponseWriter, r *http.Request) {
	// read maxSize from body
	r.Body = http.MaxBytesReader(w, r.Body, maxSize)

	defer r.Body.Close()

	hash := r.FormValue("hash")
	token := r.FormValue("token")
	if !checkSecurity(hash, token) {
		returnError(w, http.StatusUnauthorized, "Failed security check")
		return
	}

	// maxSize on ParseMultipartFrom only works if content
	// length is given via form. When give prevents reading
	// until maxSize
	r.ParseMultipartForm(maxSize)
	file, handler, err := r.FormFile("video")
	if err != nil {
		returnError(w, http.StatusInternalServerError, "Could not parse file upload: %s", err.Error())
		return
	}
	defer file.Close()

	// creating the output file
	destName := randomString(40) + filepath.Ext(handler.Filename)
	dest, err := os.Create("./media/" + destName)
	if err != nil {
		returnError(w, http.StatusInternalServerError, "Could not open file: %s", err)
		return
	}

	// copy content over, depending on Go's current stategy this
	// is from memory or a temp file or a mix of both
	defer dest.Close()
	if _, err := io.Copy(dest, file); err != nil {
		returnError(w, http.StatusInternalServerError, "Could not copy file: %s", err)
		return
	}

	// parsing the category ID from the form
	catID, err := strconv.ParseInt(r.FormValue("category"), 10, 64)
	if err != nil {
		returnError(w, http.StatusInternalServerError, "Misformated category ID: %s", r.FormValue("category"))
		return
	}

	var category database.Category
	db := getDatabase(r)

	res := db.First(&category, catID)

	// invalid category ID
	if errors.Is(res.Error, gorm.ErrRecordNotFound) {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// generate thumbnails
	wg := sync.WaitGroup{}
	lock := sync.Mutex{}
	thumbs := make(map[int64][]byte)
	for _, size := range thumbSizes {
		wg.Add(1)
		// launch a new goroutine for every thumbnail size
		go func(size int64) {
			defer wg.Done()
			res, err := createThumbnail("./media/"+destName, size)
			if err != nil {
				log.Printf("Could not generate thumbnail in size %d: %s\n", size, err)
				return
			}
			// prevent writing to the map from multiple gorountines
			lock.Lock()
			thumbs[size] = res
			lock.Unlock()
		}(size)
	}
	// wait until all thumbnails are generated
	wg.Wait()

	// save the record for the video
	video := database.Video{
		Title:            r.FormValue("title"),
		OriginalFileName: handler.Filename,
		FileName:         destName,
		Category:         category,
		Thumbnail64:      thumbs[thumbSizes[0]],
		Thumbnail128:     thumbs[thumbSizes[1]],
		Thumbnail256:     thumbs[thumbSizes[2]],
	}
	db.Create(&video)

	retireToken(token)

	// this seems illegal/dumb, but somehow is only way to redirect from a POST
	// page to page that only accepts GET request
	http.Redirect(w, r, "/", http.StatusFound)
}

// index renders the index page with all the available videos
func index(w http.ResponseWriter, r *http.Request) {
	var videos []database.Video
	getDatabase(r).Joins("Category").Find(&videos)
	renderHTML(w, "index", map[string]interface{}{
		"Videos": videos,
	})
}
