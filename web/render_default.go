//go:build !production
// +build !production

package web

import (
	"fmt"
	"html/template"
)

// getTemplate parses a template and all layout and partials into
// an executable template
func getTemplate(name string) (*template.Template, error) {
	pages, layouts, err := listFiles()
	if err != nil {
		return nil, err
	}
	if _, exists := pages[name]; !exists {
		return nil, fmt.Errorf("could not find template %s", name)
	}
	files := append([]string{pages[name]}, layouts...)
	return template.ParseFiles(files...)
}
