package web

// I'm not sure how this whole signaling stuff works on windows
// so this is a broken down naive implementation that just starts the server
// cleanup will not be executed, timeout is ignored completely

import (
	"log"
	"net/http"
	"time"
)

// StartServer starts a http.Server
func StartServer(server http.Server, timeout time.Duration, cleanup func()) {

	go func() {
		// starting the server in a separate go routine, this blocks
		// as long as the server is running
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Could not bind server: %s\n", err)
		}
	}()
	log.Println("Started server")
}
