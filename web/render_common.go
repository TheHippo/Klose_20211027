package web

import (
	"log"
	"net/http"
	"os"
	"strings"
)

const dir = "./html/"

// listFiles reads the html folder with templates. It return all page
// templates (they have ".page.html" in their name) and all layouts and
// partials
func listFiles() (map[string]string, []string, error) {
	files, err := os.ReadDir(dir)
	if err != nil {
		return nil, nil, err
	}
	pages := map[string]string{}
	layouts := []string{}
	for _, file := range files {
		if strings.Contains(file.Name(), ".page.html") {
			pages[strings.TrimSuffix(file.Name(), ".page.html")] = dir + file.Name()
		} else {
			layouts = append(layouts, dir+file.Name())
		}
	}
	return pages, layouts, nil
}

// renderHTML renders a template by its name on a HTTP response
func renderHTML(w http.ResponseWriter, name string, data interface{}) {
	tmpl, err := getTemplate(name)
	if err != nil {
		w.Header().Set("Content-Type", "text/plain; charset=ut-8")
		w.WriteHeader(http.StatusInternalServerError)
		// giving the error directly to the user is bad practice
		// so if this ever goes live fix this before
		w.Write([]byte(err.Error()))
		log.Printf("Could not get template: %s\n", name)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := tmpl.Execute(w, data); err != nil {
		log.Printf("Error while executing template %s: %v", name, err)
	}
}
