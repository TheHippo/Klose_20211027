package main

import (
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/TheHippo/Klose_20211027/database"
	"gitlab.com/TheHippo/Klose_20211027/web"
)

func main() {
	log.Println("Opening database")

	db := database.OpenAndPrepareDatabase()

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.RequestID)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Compress(5))

	r = web.BuildRoutes(r, db)

	server := http.Server{
		Addr:    ":3000",
		Handler: r,
	}

	web.StartServer(server, 2*time.Second, func() {
		sqlDb, err := db.DB()
		if err == nil {
			// not all supported database are closable
			sqlDb.Close()
		}
	})

}
