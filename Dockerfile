# Build backend
FROM golang:1.17 AS gobuild
WORKDIR /app
ARG GOTAGS=production,postgres
COPY go.* /app/
RUN go mod download
COPY . /app/
RUN go build -v -ldflags="-s -w" -tags=${GOTAGS} -o server gitlab.com/TheHippo/Klose_20211027

# Build frontend
FROM debian:11-slim as frontendbuild
WORKDIR /app
RUN apt-get update && apt-get install curl ca-certificates gnupg --yes --no-install-recommends && \
	curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor > /usr/share/keyrings/yarn.gpg && \
	echo "deb [signed-by=/usr/share/keyrings/yarn.gpg] https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
	apt-get update && apt-get install yarn nodejs --yes --no-install-recommends && \
	yarn global add npx
COPY . /app/
RUN yarn && yarn js:build:production && yarn css:build:production

# Final image
FROM debian:11-slim
EXPOSE 3000
WORKDIR /app
RUN apt-get update && apt-get install wget ca-certificates xz-utils --yes --no-install-recommends && \
	mkdir -p /opt/ffmpeg  && \
	cd /opt/ffmpeg  && \
	wget https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz  && \
	tar xvf ffmpeg*.xz  && \
	cd ffmpeg-*-static  && \
	cp "${PWD}/ffmpeg" /usr/local/bin/  && \
	cd  && \
	rm /opt/ffmpeg -rfv && \
	apt-get remove wget xz-utils --auto-remove --yes && apt-get clean && \
	mkdir /app/media/
COPY --from=gobuild /app/server /app/server
COPY --from=frontendbuild /app/static/* /app/static/
COPY html/*.html /app/html/
CMD ["./server"]


