document.addEventListener("DOMContentLoaded", () => {
	
	// gray overlay behind the video
	let playerOverlay = document.querySelector('.video-player');
	// actuall vido player
	let video = document.querySelector('.video-player video');
	// close button
	let cross = document.querySelector('.video-player .cross');

	cross.addEventListener('click', () => {
		stopPlayback();
	})

	playerOverlay.addEventListener('click', (e) => {
		if (e.target == e.currentTarget) {
			// don't react to clicks of the actual player
			stopPlayback();
		}
	})

	var isPlaying = false;

	function startPlayback(videoUrl) {
		if (!isPlaying) {
			playerOverlay.style.display = 'block';
			video.setAttribute('src', videoUrl)
			video.play();

			isPlaying = true;
		}
	}

	function stopPlayback() {
		if (isPlaying) {
			playerOverlay.style.display = 'none';
			video.pause();
			isPlaying = false;
		}
	}

	document.addEventListener('keydown', (e) => {
		if (e.keyCode == 27 && isPlaying) {
			// why is keyCode deprecated?
			stopPlayback()
		}
	});

	// all the elements that could trigger playing a video
	let clickable =  document.querySelectorAll("[data-play]");
	clickable.forEach((element => {
		element.addEventListener('click', (e) => {
			e.preventDefault(); // prevent the download on the play buttons
			startPlayback(element.getAttribute('data-play'));
		})
	}))
})